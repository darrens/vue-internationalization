import Vue from 'vue'
import App from './App.vue'
import ElementUI from 'element-ui'
import i18n from './lang'
Vue.use(ElementUI, {
  i18n: (key, value) => i18n.t(key, value)
})
// Vue.config.productionTip = false
new Vue({
  i18n,
  render: h => h(App),
}).$mount('#app')

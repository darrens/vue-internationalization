import Vue from 'vue'
import enLocale from 'element-ui/lib/locale/lang/en'
import zhLocale from 'element-ui/lib/locale/lang/zh-CN'
import zhContent from './zh'
import enContent from './en'
import VueI18n from 'vue-i18n'
Vue.use(VueI18n)
const messages = {
    en: {
     ...enContent,
      ...enLocale // 或者用 Object.assign({ message: 'hello' }, enLocale)
    },
    zh: {
      ...zhContent,
      ...zhLocale // 或者用 Object.assign({ message: '你好' }, zhLocale)
    }
  }
  // Create VueI18n instance with options
const i18n = new VueI18n({
    locale: 'en', // set locale
    messages, // set locale messages
  })

  export default i18n